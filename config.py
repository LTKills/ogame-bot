import configparser
import os
import sys
import watchdog
import shutil
from watchdog.observers import Observer

global options

class Options(watchdog.events.FileSystemEventHandler):
    REQUIRED_SECTIONS = {
        'credentials',
        'general',
        'building',
        'research',
        'station',
        'fleet'
    }

    def __init__(self, *args, **kwarsg):

        if getattr(sys, 'frozen', False):
            self.application_path = os.path.dirname(sys.executable)
            self.CONFIG_FILE = self.application_path + r'\config.ini'

            if not os.path.isfile(self.CONFIG_FILE):
                shutil.copy(sys._MEIPASS + r'\config.ini.sample',self.application_path + r'\config.ini')
        else:
            self.application_path = os.path.dirname(os.path.abspath(__file__))
            self.CONFIG_FILE = self.application_path + r'/config.ini'

        self._config = configparser.ConfigParser()
        self.paused = False
        self.reload_config()
        self._config_valid = False
        self.observer = Observer()


        self.observer.schedule(self, path=self.application_path, recursive=False)
        self.observer.start()

    def __del__(self):
        self.observer.stop()
        self.observer.join()

    def on_any_event(self, event):
        if self.CONFIG_FILE in event.src_path:
            self.reload_config()

    def reload_config(self):
        if not self.paused:
            print("config reloaded")
            self._config.read(self.CONFIG_FILE)
            self.check_config()

    def check_config(self):
        self._config_valid = False

        try:
            for section in self.REQUIRED_SECTIONS:
                self._config.items(section)
            self._config_valid = True
        except ConfigParser.NoSectionError:
            self._config_valid = False

    def __getitem__(self, section):

        return dict(self._config.items(section))

    def set(self, section, key, value):
        self.paused = True
        self._config.set(section, key, value)
        with open(self.CONFIG_FILE, 'w') as f:
                self._config.write(f)

        self.paused = False

    @property
    def valid(self):
        return self._is_config_valid

options = Options()
